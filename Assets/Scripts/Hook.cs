﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class Hook : MonoBehaviour
{
    [SerializeField]
    private LayerMask hitMask;
    [SerializeField]
    private float range = 10f;
    [SerializeField]
    private float climbSpeed = 5f;
    private Vector2 mousePosition;
    private Transform playerTransform;
    private Vector2 hookPosition = Vector2.zero;
    private SpringJoint2D rope;
    private LineRenderer ropeLine;

    private void Start()
    {
        playerTransform = this.transform;
        ropeLine = this.GetComponent<LineRenderer>();
    }

    private void Update()
    {
        checkInput();
        if (rope != null)
            DrawRope();

    }

    private void checkInput()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0) && rope == null)
        {
            Shoot();
        }
        else if (Input.GetKeyDown(KeyCode.Mouse0) && rope != null)
        {
            Destroy(rope);
            ropeLine.enabled = false;
        }
        float direccion = Input.GetAxis("Vertical");
        if (direccion != 0 && rope != null)
        {
            Climb(direccion);
        }
    }

    private void Climb(float direction)
    {
        rope.distance += direction * Time.deltaTime * climbSpeed;
    }
    private Vector2 CalcularDireccion()
    {
        mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        return mousePosition - (Vector2)playerTransform.position;
    }

    private void CreateRope(Rigidbody2D connectedHook)
    {
        rope = this.gameObject.AddComponent<SpringJoint2D>();
        rope.connectedBody = connectedHook;
        hookPosition = connectedHook.transform.position;
        rope.distance = Vector3.Magnitude(hookPosition - (Vector2)playerTransform.position);
        rope.dampingRatio = 1f;
        rope.frequency = 0f;
    }

    private void DrawRope()
    {
        ropeLine.numPositions = 2;
        ropeLine.SetPosition(0, playerTransform.position);
        ropeLine.SetPosition(1, hookPosition);
    }

    private void Shoot()
    {

        RaycastHit2D hitInfo = Physics2D.CircleCast(playerTransform.position, 1, CalcularDireccion(), range, hitMask);
        if (hitInfo && rope == null)
        {
            CreateRope(hitInfo.rigidbody);
            ropeLine.enabled = true;
        }
        else
            return;
    }
}
