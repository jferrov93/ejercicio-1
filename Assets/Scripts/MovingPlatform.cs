﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum typeOfMovement
{
	single,loop,pingPong
}
public class MovingPlatform : MonoBehaviour 
{
    [SerializeField]
    private List<Transform> wayPoints;

    [SerializeField]
    private float travelTime = 1f;
	 
	[SerializeField]
	private  typeOfMovement tipo;

    private float timePassed;

    private int currentWayPoint = 0;

	private int nextWayPoint = 1;

	private void Start()
	{
		nextWayPoint = currentWayPoint + 1;
	}

    private void Update()
    {
        Move();
    }

    private void Move()
    {
		timePassed += Time.deltaTime;
		transform.position = Vector2.Lerp(wayPoints[currentWayPoint].position, wayPoints[nextWayPoint].position, timePassed / travelTime);
        if (timePassed >= travelTime)
            OnFinished();
    }

    private void OnFinished()
    {
		switch (tipo) 
		{
		case typeOfMovement.single:
			nextWayPointSingle ();
			break;
		case typeOfMovement.loop:
			nextWayPointLoop ();
			break;
		case typeOfMovement.pingPong:
			nextWayPointPingPong ();
			break;

		}
    }

	private void nextWayPointSingle()
	{
		if (currentWayPoint + 2 < wayPoints.Count) 
		{
			currentWayPoint += 1;
			nextWayPoint += 1;
			timePassed = 0f;
		}
	}

	private void nextWayPointLoop()
	{
		if (currentWayPoint + 2 < wayPoints.Count) 
		{
			currentWayPoint += 1;
			nextWayPoint += 1;
		}
		else if (nextWayPoint != 0) 
		{
			currentWayPoint = wayPoints.Count - 1;
			nextWayPoint = 0;
		} 
		else  
		{
			currentWayPoint = 0;
			nextWayPoint = 1;
		}

        timePassed = 0f;
    }

	private void nextWayPointPingPong()
	{
        bool avanzando = (currentWayPoint < nextWayPoint);

        if (avanzando && currentWayPoint + 2 < wayPoints.Count)
        {
            currentWayPoint += 1;
            nextWayPoint += 1;
        }
        else if (avanzando && nextWayPoint == wayPoints.Count-1)
        {
            currentWayPoint = wayPoints.Count - 1;
            nextWayPoint = currentWayPoint-1;
        }
        else if (!avanzando && currentWayPoint > 1)
        {
            currentWayPoint -= 1;
            nextWayPoint -= 1;
        }
        else
        {
            currentWayPoint = 0;
            nextWayPoint = 1;
        }


        timePassed = 0f;

    }


    
    
}
